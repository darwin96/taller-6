all: taller
taller: taller6_bad.o
	gcc taller6_bad.o -o taller

taller6_bad.o: taller6_bad.c
	gcc -Wall -c taller6_bad.c -o taller6_bad.o
.PHONY: clean
clean:
	rm taller_bad.o taller
